package com.nipa.ticket.test.service;

import java.util.Date;
import java.util.List;

import com.nipa.ticket.test.dto.TicketBody;
import com.nipa.ticket.test.dto.TicketInfo;
import com.nipa.ticket.test.dto.TicketStatus;

public interface ITicketService {
		
		TicketInfo addTicket(TicketBody ticket);
		
		List<TicketInfo> getTicket(List<TicketStatus> status, Date start, Date end, Integer offset, Integer limit, String sort);
		
		TicketInfo updateTicket(Long id, TicketStatus status);
}
