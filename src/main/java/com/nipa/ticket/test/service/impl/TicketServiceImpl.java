package com.nipa.ticket.test.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.nipa.ticket.test.config.JPAQueryProperties;
import com.nipa.ticket.test.dto.TicketBody;
import com.nipa.ticket.test.dto.TicketInfo;
import com.nipa.ticket.test.dto.TicketStatus;
import com.nipa.ticket.test.model.TicketEntity;
import com.nipa.ticket.test.repository.TicketRepository;
import com.nipa.ticket.test.service.ITicketService;
import com.nipa.ticket.test.utils.JPAQueryUtils;
import com.nipa.ticket.test.utils.TicketUtils;

@Service("ticket/TicketServiceImpl")
public class TicketServiceImpl implements ITicketService {

	private static final Logger log = LoggerFactory.getLogger(TicketServiceImpl.class);

	@Autowired
	private TicketRepository ticketRepo;
	
	@Autowired
	private JPAQueryProperties jpaProp;

	@Override
	public TicketInfo addTicket(TicketBody ticket) {

		log.info("Create ticket.");
		TicketEntity entity = TicketUtils.getTicketEntity(ticket);
		entity.setStatus(TicketStatus.pending);
		entity.setCreateDate(new Date());

		ticketRepo.save(entity);

		return TicketUtils.getTicketInfo(entity);
	}

	@Override
	public List<TicketInfo> getTicket(List<TicketStatus> status, Date start, Date end, Integer offset, Integer limit,
			String sort) {

		Pageable pageable = getPageable(offset, limit, sort);

		List<TicketEntity> ticket = new ArrayList<TicketEntity>();
		if (status != null && !status.isEmpty()) {
			if (start != null || end != null) {
				if (end == null) {
					ticket = ticketRepo.findTicketByStatusAndDateStart(status, start, pageable);
				} else if (start == null) {
					ticket = ticketRepo.findTicketByStatusAndDateEnd(status, end, pageable);
				} else {
					ticket = ticketRepo.findTicketByStatusAndDate(status, start, end, pageable);
				}
			} else {
				ticket = ticketRepo.findTicketByStatus(status, pageable);
			}
		} else {
			if (start != null || end != null) {
				if (end == null) {
					ticket = ticketRepo.findTicketByDateStart(start, pageable);
				} else if (start == null) {
					ticket = ticketRepo.findTicketByDateEnd(end, pageable);
				} else {
					ticket = ticketRepo.findTicketByDate(start, end, pageable);
				}
			} else {
				ticket = ticketRepo.findTicket(pageable);
			}
		}

		return TicketUtils.getTicketInfo(ticket);
	}

	@Override
	public TicketInfo updateTicket(Long id, TicketStatus status) {

		log.info("Update ticket status by id {}.", id);
		TicketEntity ticket = ticketRepo.findTicketById(id);
		if (ticket == null) {
			log.info("Ticket id {} not found.", id);
			return null;
		}
		ticket.setStatus(status);
		ticket.setUpdateDate(new Date());
		ticketRepo.save(ticket);

		return TicketUtils.getTicketInfo(ticket);
	}
	
	/**
	 * Other Service
	 */
	public Pageable getPageable(Integer offset, Integer limit, String sort) {

		if (offset == null) {
			offset = jpaProp.getOffset();
		}
		if (limit == null) {
			limit = jpaProp.getLimit();
		}

		if (sort == null || sort.isEmpty()) {
			return PageRequest.of(offset, limit);
		}

		List<Sort.Order> orders = JPAQueryUtils.getSort(sort);
		return PageRequest.of(offset, limit, Sort.by(orders));
	}

}
