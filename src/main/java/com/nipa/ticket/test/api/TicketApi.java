package com.nipa.ticket.test.api;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.nipa.ticket.test.dto.TicketBody;
import com.nipa.ticket.test.dto.TicketInfo;
import com.nipa.ticket.test.dto.TicketStatus;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-02-06T04:02:02.236Z[GMT]")
public interface TicketApi {

    @Operation(summary = "สร้าง ticket", description = "สร้าง ticket", tags={ "Ticket" })
    @ApiResponses(value = { 
        @ApiResponse(responseCode = "200", description = "OK", content = @Content(schema = @Schema(implementation = TicketInfo.class))),
        
        @ApiResponse(responseCode = "400", description = "คำร้องขอที่ส่งมามีความผิดพลาดทางไวยากรณ์ หรือไม่สามารถทำตามการร้องขอนั้นได้"),
        
        @ApiResponse(responseCode = "401", description = "ไม่ได้รับอนุญาต การเข้าถึงถูกปฏิเสธเนื่องจากการพิสูจน์ตัวตนที่ไม่ถูกต้อง"),
        
        @ApiResponse(responseCode = "403", description = "การพิสูจน์ตัวตนถูกต้อง แต่ไม่ได้รับการอนุญาตให้เข้าถึง resource นี้"),
        
        @ApiResponse(responseCode = "500", description = "เกิดข้อผิดพลาดภายในเซิร์ฟเวอร์"),
        
        @ApiResponse(responseCode = "503", description = "บริการไม่พร้อมใช้งาน") })
    @RequestMapping(value = "/ticket",
        produces = { "application/json" }, 
        consumes = { "application/json" }, 
        method = RequestMethod.POST)
    ResponseEntity<TicketInfo> addTicket(@Parameter(in = ParameterIn.DEFAULT, description = "", schema=@Schema()) @Valid @RequestBody TicketBody body);


    @Operation(summary = "ค้นหา Ticket", description = "ค้นหา Ticket", tags={ "Ticket" })
    @ApiResponses(value = { 
        @ApiResponse(responseCode = "200", description = "OK", content = @Content(array = @ArraySchema(schema = @Schema(implementation = TicketInfo.class)))),
        
        @ApiResponse(responseCode = "400", description = "คำร้องขอที่ส่งมามีความผิดพลาดทางไวยากรณ์ หรือไม่สามารถทำตามการร้องขอนั้นได้"),
        
        @ApiResponse(responseCode = "401", description = "ไม่ได้รับอนุญาต การเข้าถึงถูกปฏิเสธเนื่องจากการพิสูจน์ตัวตนที่ไม่ถูกต้อง"),
        
        @ApiResponse(responseCode = "403", description = "การพิสูจน์ตัวตนถูกต้อง แต่ไม่ได้รับการอนุญาตให้เข้าถึง resource นี้"),
        
        @ApiResponse(responseCode = "500", description = "เกิดข้อผิดพลาดภายในเซิร์ฟเวอร์"),
        
        @ApiResponse(responseCode = "503", description = "บริการไม่พร้อมใช้งาน") })
    @RequestMapping(value = "/ticket",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    ResponseEntity<List<TicketInfo>> findTickets(@Parameter(in = ParameterIn.QUERY, description = "สถานะของ ticket" ,schema=@Schema(allowableValues={ "pending", "accepted", "resolved", "rejected" }
)) @Valid @RequestParam(value = "status", required = false) List<TicketStatus> status, 
    		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") @Parameter(in = ParameterIn.QUERY, description = "เวลาเริ่ม yyyy-MM-dd HH:mm:ss" ,schema=@Schema()) @Valid @RequestParam(value = "start", required = false) Date start, 
    		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") @Parameter(in = ParameterIn.QUERY, description = "เวลาสิ้นสุด yyyy-MM-dd HH:mm:ss" ,schema=@Schema()) @Valid @RequestParam(value = "end", required = false) Date end, 
    		@Min(0)@Parameter(in = ParameterIn.QUERY, description = "ตำแหน่งเริ่มต้นของรายการที่ต้องการให้แสดง" ,schema=@Schema(allowableValues={  }
, defaultValue="0")) @Valid @RequestParam(value = "offset", required = false, defaultValue="0") Integer offset, @Min(0)@Parameter(in = ParameterIn.QUERY, description = "จำนวนรายการที่จะแสดง" ,schema=@Schema(allowableValues={  }
)) @Valid @RequestParam(value = "limit", required = false) Integer limit, @Parameter(in = ParameterIn.QUERY, description = "sort" ,schema=@Schema()) @Valid @RequestParam(value = "sort", required = false) String sort);


    @Operation(summary = "เปลี่ยนสถานะของ ticket", description = "เปลี่ยนสถานะของ ticket", tags={ "Ticket" })
    @ApiResponses(value = { 
        @ApiResponse(responseCode = "200", description = "OK"),
        
        @ApiResponse(responseCode = "400", description = "คำร้องขอที่ส่งมามีความผิดพลาดทางไวยากรณ์ หรือไม่สามารถทำตามการร้องขอนั้นได้"),
        
        @ApiResponse(responseCode = "401", description = "ไม่ได้รับอนุญาต การเข้าถึงถูกปฏิเสธเนื่องจากการพิสูจน์ตัวตนที่ไม่ถูกต้อง"),
        
        @ApiResponse(responseCode = "403", description = "การพิสูจน์ตัวตนถูกต้อง แต่ไม่ได้รับการอนุญาตให้เข้าถึง resource นี้"),
        
        @ApiResponse(responseCode = "500", description = "เกิดข้อผิดพลาดภายในเซิร์ฟเวอร์"),
        
        @ApiResponse(responseCode = "503", description = "บริการไม่พร้อมใช้งาน") })
    @RequestMapping(value = "/ticket/status/id/{id}",
        produces = { "application/json" }, 
        method = RequestMethod.PATCH)
    ResponseEntity<Void> updateStatus(@Parameter(in = ParameterIn.PATH, description = "id ของ ticket", required=true, schema=@Schema()) @PathVariable("id") Long id, @NotNull @Parameter(in = ParameterIn.QUERY, description = "สถานะของ ticket" ,required=true,schema=@Schema(allowableValues={ "pending", "accepted", "resolved", "rejected" }
)) @Valid @RequestParam(value = "status", required = true) TicketStatus status);

}
