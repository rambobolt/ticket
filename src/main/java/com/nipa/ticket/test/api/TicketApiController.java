package com.nipa.ticket.test.api;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.server.ResponseStatusException;

import com.nipa.ticket.test.dto.TicketBody;
import com.nipa.ticket.test.dto.TicketInfo;
import com.nipa.ticket.test.dto.TicketStatus;
import com.nipa.ticket.test.service.ITicketService;

import io.swagger.annotations.Api;

@Api(value = "Ticket", tags = { "Ticket" })
@Controller
public class TicketApiController implements TicketApi {

    private static final Logger log = LoggerFactory.getLogger(TicketApiController.class);
    
    @Autowired
    @Qualifier("ticket/TicketServiceImpl")
    private ITicketService ticketService;

	@Override
	public ResponseEntity<TicketInfo> addTicket( TicketBody body) {
		
		try {
			
			TicketInfo ticket = ticketService.addTicket(body);
			return new ResponseEntity<TicketInfo>(ticket, HttpStatus.OK);
		} catch (Exception e) {
			
			log.error("Fail to create ticket", e);
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getLocalizedMessage());
		}
	}

	@Override
	public ResponseEntity<List<TicketInfo>> findTickets( List<TicketStatus> status, Date start, Date end, Integer offset, Integer limit, String sort) {
	
		try {

			List<TicketInfo> ticket = ticketService.getTicket(status, start, end, offset, limit, sort);
			return new ResponseEntity<List<TicketInfo>>(ticket, HttpStatus.OK);
		} catch (Exception e) {
			
			log.error("Fail to find ticket", e);
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getLocalizedMessage());
		}
	}

	@Override
	public ResponseEntity<Void> updateStatus(Long id, TicketStatus status) {

		try {

			ticketService.updateTicket(id, status);	
			return new ResponseEntity<Void>(HttpStatus.OK);
		} catch (Exception e) {
			
			log.error("Fail to find ticket", e);
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getLocalizedMessage());
		}
	}



}
