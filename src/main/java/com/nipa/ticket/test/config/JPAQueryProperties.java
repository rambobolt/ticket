package com.nipa.ticket.test.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JPAQueryProperties {
	
	@Value("${spring.jpa.query.offset:0}")
	private Integer offset;
	
	@Value("${spring.jpa.query.limit:100}")
	private Integer limit;

	public Integer getOffset() {
		return offset;
	}

	public void setOffset(Integer offset) {
		this.offset = offset;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

}
