package com.nipa.ticket.test.config;

import java.util.Collections;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

	@Value("${swagger.api-info.title}")
	private String title;
	
	@Value("${swagger.api-info.description}")
	private String description;
	
	@Value("${swagger.api-info.license}")
	private String license;
	
	@Value("${swagger.api-info.license-uri}")
	private String licenseUrl;
	
	@Value("${swagger.api-info.version}")
	private String version;
	
	@Value("${swagger.api-info.contact.name}")
	private String contactName;
	
	@Value("${swagger.api-info.contact.url}")
	private String contactUrl;
	
	@Value("${swagger.api-info.contact.email}")
	private String contactEmail;
	
	private ApiInfo apiInfo() {
		return new ApiInfo(title, description, version,
				"API Terms of Service URL", new Contact(contactName, contactUrl, contactEmail),
				license, licenseUrl, Collections.emptyList());
	}

	@Bean
	public Docket apiDocket() {

		Docket docket = new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage("com.nipa.ticket.test.api")).paths(PathSelectors.any())
				.build().apiInfo(apiInfo());

		return docket;

	}
}
