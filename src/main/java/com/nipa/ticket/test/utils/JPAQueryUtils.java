package com.nipa.ticket.test.utils;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.Sort;

public class JPAQueryUtils {

	public static List<Sort.Order> getSort(String c) {
		List<Sort.Order> sorts = new ArrayList<>();
		if (c == null) {
			return sorts;
		}

		if (c.startsWith("-")) {
			sorts.add(Sort.Order.desc(c.substring(c.indexOf("-") + 1)));
		} else if (c.startsWith("+")) {
			sorts.add(Sort.Order.asc(c.substring(c.indexOf("+") + 1)));
		} else {
			sorts.add(Sort.Order.asc(c));
		}

		return sorts;
	}

}
