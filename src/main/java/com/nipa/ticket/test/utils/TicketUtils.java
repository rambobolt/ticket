package com.nipa.ticket.test.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.nipa.ticket.test.dto.TicketBody;
import com.nipa.ticket.test.dto.TicketInfo;
import com.nipa.ticket.test.dto.TicketStatus;
import com.nipa.ticket.test.model.TicketEntity;

public class TicketUtils {
	
	//Ticket entity builder
	public static final TicketEntityBuilder getTicketEntityBuilder() {
		return new TicketEntityBuilder();
	}
	
	public static final TicketEntityBuilder getTicketEntityBuilder(TicketBody body) {
		return new TicketEntityBuilder(body);
	}

	public static final TicketEntity getTicketEntity(TicketBody body) {
		
		if(body == null) {
			return null;
		}
		return getTicketEntityBuilder(body).build();
	}
	
	
	//Ticket info builder
	public static final TicketInfoBuilder getTicketInfoBuilder() {
		return new TicketInfoBuilder();
	}
	
	public static final TicketInfoBuilder getTicketInfoBuilder(TicketEntity entity) {
		return new TicketInfoBuilder(entity);
	}
	
	public static final TicketInfo getTicketInfo(TicketEntity entity) {
		
		if(entity == null) {
			return null;
		}
		return  getTicketInfoBuilder(entity).build();
	}
	
	public static final List<TicketInfo> getTicketInfo(List<TicketEntity> entity ) {
		
		List<TicketInfo> dtos = new ArrayList<TicketInfo>();
		
		entity.forEach(t -> dtos.add(getTicketInfoBuilder(t).build()));
		
		return dtos;
	}
	
	public static final class TicketEntityBuilder {
		
		private final TicketEntity entity;
		
		private TicketEntityBuilder() {
			entity = new TicketEntity();
		}
		
		private TicketEntityBuilder(TicketBody body) {
			
			entity = new TicketEntity();
			
			setName(body.getName())
			.setDetail(body.getDetail())
			.setContact(body.getContact());
		}

		public TicketEntityBuilder setName(String name) {
			entity.setName(name);
			return this;
		}

		public TicketEntityBuilder setDetail(String detail) {
			entity.setDetail(detail);
			return this;
		}

		public TicketEntityBuilder setContact(String contact) {
			entity.setContact(contact);
			return this;
		}

		public TicketEntityBuilder setStatus(TicketStatus status) {
			entity.setStatus(status);
			return this;
		}

		public TicketEntityBuilder setCreateDate(Date createdate) {
			entity.setCreateDate(createdate);
			return this;
		}

		public TicketEntityBuilder setUpdateDate(Date updatedate) {
			entity.setUpdateDate(updatedate);
			return this;
		}
		
		public TicketEntity build() {
			return this.entity;
		}
	}
	
	public static final class TicketInfoBuilder {
		
		private final TicketInfo info;

		private TicketInfoBuilder() {
			info = new TicketInfo();
		}
		
		private TicketInfoBuilder(TicketEntity entity) {
			info = new TicketInfo();

			setId(entity.getId())
			.setName(entity.getName())
			.setDetail(entity.getDetail())
			.setContact(entity.getContact())
			.setStatus(entity.getStatus())
			.setCreateDate(entity.getCreateDate())
			.setUpdateDate(entity.getUpdateDate());
		}

		public TicketInfoBuilder setId(Long id) {
			info.setId(id);
			return this;
		}

		public TicketInfoBuilder setName(String name) {
			info.setName(name);
			return this;
		}

		public TicketInfoBuilder setDetail(String detail) {
			info.setDetail(detail);
			return this;
		}

		public TicketInfoBuilder setContact(String contact) {
			info.setContact(contact);
			return this;
		}

		public TicketInfoBuilder setStatus(TicketStatus status) {
			info.setStatus(status);
			return this;
		}

		public TicketInfoBuilder setCreateDate(Date createDate) {
			info.setCreateDate(createDate);
			return this;
		}

		public TicketInfoBuilder setUpdateDate(Date updateDate) {
			info.setUpdateDate(updateDate);
			return this;
		}
		
		public TicketInfo build() {
			return this.info;
		}
	}
}
