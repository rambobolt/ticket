package com.nipa.ticket.test.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.nipa.ticket.test.dto.TicketStatus;
import com.nipa.ticket.test.model.TicketEntity;

@Repository
public interface TicketRepository extends JpaRepository<TicketEntity, Long> {
	
	
	@Query("SELECT t FROM TicketEntity t WHERE t.id = :ID")
	TicketEntity findTicketById(@Param("ID") Long id);
	
	@Query("SELECT t FROM TicketEntity t")
	List<TicketEntity> findTicket(Pageable pageable);
	
	@Query("SELECT t FROM TicketEntity t WHERE t.createDate BETWEEN :START AND :END")
	List<TicketEntity> findTicketByDate(@Param("START") Date start, @Param("END") Date end, Pageable pageable);
	
	@Query("SELECT t FROM TicketEntity t WHERE t.createDate <= :END")
	List<TicketEntity> findTicketByDateEnd(@Param("END") Date end, Pageable pageable);
	
	@Query("SELECT t FROM TicketEntity t WHERE t.createDate >= :START")
	List<TicketEntity> findTicketByDateStart(@Param("START") Date start, Pageable pageable);
	
	
	/**
	 * Get By Status
	 */
	@Query("SELECT t FROM TicketEntity t WHERE t.status in :STATUS")
	List<TicketEntity> findTicketByStatus(@Param("STATUS") List<TicketStatus> status, Pageable pageable);
	
	@Query("SELECT t FROM TicketEntity t WHERE t.status in :STATUS AND t.createDate BETWEEN :START AND :END")
	List<TicketEntity> findTicketByStatusAndDate(@Param("STATUS") List<TicketStatus> status, @Param("START") Date start, @Param("END") Date end, Pageable pageable);
	
	@Query("SELECT t FROM TicketEntity t WHERE t.status in :STATUS AND t.createDate <= :END")
	List<TicketEntity> findTicketByStatusAndDateEnd(@Param("STATUS") List<TicketStatus> status, @Param("END") Date end, Pageable pageable);
	
	@Query("SELECT t FROM TicketEntity t WHERE t.status in :STATUS AND t.createDate >= :START")
	List<TicketEntity> findTicketByStatusAndDateStart(@Param("STATUS") List<TicketStatus> status, @Param("START") Date start, Pageable pageable);
	
}
