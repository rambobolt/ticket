package com.nipa.ticket.test.dto;

public enum TicketStatus {

	pending("PENDING"), accepted("ACCEPTED"), resolved("RESOLVED"), rejected("REJECTED");
	
	private final String keyword;
	
	private TicketStatus(String keyword) {
		this.keyword = keyword;
	}

	public String getValue() {
		return keyword;
	}
	
}
