package com.nipa.ticket.test.dto;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.validation.annotation.Validated;
import javax.validation.constraints.*;

/**
 * ข้อมูล ticket
 */
@Schema(description = "ข้อมูล ticket")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-02-06T04:02:02.236Z[GMT]")


public class TicketBody   {
  @JsonProperty("name")
  private String name = null;

  @JsonProperty("detail")
  private String detail = null;

  @JsonProperty("contact")
  private String contact = null;

  public TicketBody name(String name) {
    this.name = name;
    return this;
  }

  /**
   * ชื่อ
   * @return name
   **/
  @Schema(required = true, description = "ชื่อ")
      @NotNull

    public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public TicketBody detail(String detail) {
    this.detail = detail;
    return this;
  }

  /**
   * รายละเอียด
   * @return detail
   **/
  @Schema(required = true, description = "รายละเอียด")
      @NotNull

    public String getDetail() {
    return detail;
  }

  public void setDetail(String detail) {
    this.detail = detail;
  }

  public TicketBody contact(String contact) {
    this.contact = contact;
    return this;
  }

  /**
   * ข้อมูลการติดต่อ
   * @return contact
   **/
  @Schema(required = true, description = "ข้อมูลการติดต่อ")
      @NotNull

    public String getContact() {
    return contact;
  }

  public void setContact(String contact) {
    this.contact = contact;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TicketBody ticketBody = (TicketBody) o;
    return Objects.equals(this.name, ticketBody.name) &&
        Objects.equals(this.detail, ticketBody.detail) &&
        Objects.equals(this.contact, ticketBody.contact);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, detail, contact);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TicketBody {\n");
    
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    detail: ").append(toIndentedString(detail)).append("\n");
    sb.append("    contact: ").append(toIndentedString(contact)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}