package com.nipa.ticket.test.dto;

import java.util.Date;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ข้อมูล ticket
 */
@Schema(description = "ข้อมูล ticket")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-02-06T04:02:02.236Z[GMT]")


public class TicketInfo   {
  @JsonProperty("id")
  private Long id = null;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("detail")
  private String detail = null;

  @JsonProperty("contact")
  private String contact = null;

  @JsonProperty("status")
  private TicketStatus status = null;

  @JsonProperty("createDate")
  private Date createDate = null;

  @JsonProperty("updateDate")
  private Date updateDate = null;

  public TicketInfo id(Long id) {
    this.id = id;
    return this;
  }

  /**
   * id ของ ticket
   * @return id
   **/
  @Schema(required = true, description = "id ของ ticket")
      @NotNull

    public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public TicketInfo name(String name) {
    this.name = name;
    return this;
  }

  /**
   * ชื่อ
   * @return name
   **/
  @Schema(required = true, description = "ชื่อ")
      @NotNull

    public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public TicketInfo detail(String detail) {
    this.detail = detail;
    return this;
  }

  /**
   * รายละเอียด
   * @return detail
   **/
  @Schema(required = true, description = "รายละเอียด")
      @NotNull

    public String getDetail() {
    return detail;
  }

  public void setDetail(String detail) {
    this.detail = detail;
  }

  public TicketInfo contact(String contact) {
    this.contact = contact;
    return this;
  }

  /**
   * ข้อมูลการติดต่อ
   * @return contact
   **/
  @Schema(required = true, description = "ข้อมูลการติดต่อ")
      @NotNull

    public String getContact() {
    return contact;
  }

  public void setContact(String contact) {
    this.contact = contact;
  }

  public TicketInfo status(TicketStatus status) {
    this.status = status;
    return this;
  }

  /**
   * สถานะ
   * @return status
   **/
  @Schema(required = true, description = "สถานะ")
      @NotNull

    public TicketStatus getStatus() {
    return status;
  }

  public void setStatus(TicketStatus status) {
    this.status = status;
  }

  public TicketInfo createDate(Date createDate) {
    this.createDate = createDate;
    return this;
  }

  /**
   * วันเวลาที่สร้าง
   * @return createDate
   **/
  @Schema(required = true, description = "วันเวลาที่สร้าง")
      @NotNull

    @Valid
    public Date getCreateDate() {
    return createDate;
  }

  public void setCreateDate(Date createDate) {
    this.createDate = createDate;
  }

  public TicketInfo updateDate(Date updateDate) {
    this.updateDate = updateDate;
    return this;
  }

  /**
   * วันเวลาที่อัพเดท
   * @return updateDate
   **/
  @Schema(required = true, description = "วันเวลาที่อัพเดท")
      @NotNull

    @Valid
    public Date getUpdateDate() {
    return updateDate;
  }

  public void setUpdateDate(Date updateDate) {
    this.updateDate = updateDate;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TicketInfo ticketInfo = (TicketInfo) o;
    return Objects.equals(this.id, ticketInfo.id) &&
        Objects.equals(this.name, ticketInfo.name) &&
        Objects.equals(this.detail, ticketInfo.detail) &&
        Objects.equals(this.contact, ticketInfo.contact) &&
        Objects.equals(this.status, ticketInfo.status) &&
        Objects.equals(this.createDate, ticketInfo.createDate) &&
        Objects.equals(this.updateDate, ticketInfo.updateDate);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, detail, contact, status, createDate, updateDate);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TicketInfo {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    detail: ").append(toIndentedString(detail)).append("\n");
    sb.append("    contact: ").append(toIndentedString(contact)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    createDate: ").append(toIndentedString(createDate)).append("\n");
    sb.append("    updateDate: ").append(toIndentedString(updateDate)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}