package com.nipa.ticket.test.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.nipa.ticket.test.dto.TicketStatus;

@Entity
@Table(name = "tb_ticket", schema = "ticket")
@SequenceGenerator(name = "ticket_id_seq", sequenceName = "tb_ticket_id_seq", allocationSize = 1, schema = "ticket")
public class TicketEntity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2411165071040053482L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ticket_id_seq")
	@Column(name = "id", nullable = true)
	private Long id;
	
	@Column(name = "name", nullable = true)
	private String name;
	
	@Column(name = "detail")
	private String detail;
	
	@Column(name ="contact")
	private String contact;
	
	@Column(name = "status", nullable = true)
	private TicketStatus status;
	
	@Column(name = "created_date", nullable = true)
	private Date createDate;
	
	@Column(name = "update_date")
	private Date updateDate = new Date();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public TicketStatus getStatus() {
		return status;
	}

	public void setStatus(TicketStatus status) {
		this.status = status;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TicketEntity other = (TicketEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TicketEntity [id=" + id + ", name=" + name + ", detail=" + detail + ", contact=" + contact + ", status="
				+ status + ", createDate=" + createDate + ", updateDate=" + updateDate + "]";
	}
}
