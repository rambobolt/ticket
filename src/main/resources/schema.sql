-- Table: ticket.tb_ticket

-- DROP TABLE ticket.tb_ticket;

CREATE TABLE ticket.tb_ticket
(
    id bigint NOT NULL DEFAULT nextval('ticket.tb_ticket_id_seq'::regclass),
    name character varying COLLATE pg_catalog."default" NOT NULL,
    detail character varying COLLATE pg_catalog."default",
    contact character varying COLLATE pg_catalog."default",
    status smallint NOT NULL,
    created_date timestamp without time zone NOT NULL,
    update_date timestamp without time zone NOT NULL,
    CONSTRAINT tb_ticket_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE ticket.tb_ticket
    OWNER to postgres;