package com.nipa.ticket.test.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import com.nipa.ticket.test.dto.TicketInfo;
import com.nipa.ticket.test.dto.TicketStatus;

@SpringBootTest
class UpdateTicketServiceTests {
	
	private static final Logger log = LoggerFactory.getLogger(UpdateTicketServiceTests.class);

	@Autowired
	@Qualifier("ticket/TicketServiceImpl")
	private ITicketService ticketService;
	
	private static final Long id1 = (long) 1;
	private static final Long id2 = (long) 2;
	private static final Long id3 = (long) 3;
	private static final Long id4 = (long) 4;
	private static final Long id99 = (long) 99;
	
	private static final TicketStatus pending = TicketStatus.pending;
	private static final TicketStatus accepted = TicketStatus.accepted;
	private static final TicketStatus resolved = TicketStatus.resolved;
	private static final TicketStatus rejected = TicketStatus.rejected;
	
	@BeforeEach
	void setUp() throws Exception {

	}

	@AfterEach
	void tearDown() throws Exception {
	}
	
	@Test
	@Transactional
	void updateticket_testcase1() {

		log.info("Start test: updateticket_testcase1");
		
		TicketInfo ticket = ticketService.updateTicket(id1, pending);
		log.info(ticket.toString());
		
		assertNotNull(ticket, "ticket must be not null");
		assertEquals(id1, ticket.getId(), "ID must be unique.");
		assertEquals(pending, ticket.getStatus(), "Status must be unique.");
		
		log.info("exec updateticket_testcase1 Service Success!");
	}
	
	@Test
	@Transactional
	void updateticket_testcase2() {

		log.info("Start test: updateticket_testcase2");
		
		TicketInfo ticket = ticketService.updateTicket(id2, accepted);
		log.info(ticket.toString());
		
		assertNotNull(ticket, "ticket must be not null");
		assertEquals(id2, ticket.getId(), "ID must be unique.");
		assertEquals(accepted, ticket.getStatus(), "Status must be unique.");
		
		log.info("exec updateticket_testcase2 Service Success!");
	}
	
	@Test
	@Transactional
	void updateticket_testcase3() {

		log.info("Start test: updateticket_testcase3");
		
		TicketInfo ticket = ticketService.updateTicket(id3, resolved);
		log.info(ticket.toString());
		
		assertNotNull(ticket, "ticket must be not null");
		assertEquals(id3, ticket.getId(), "ID must be unique.");
		assertEquals(resolved, ticket.getStatus(), "Status must be unique.");
		
		log.info("exec updateticket_testcase3 Service Success!");
	}
	
	@Test
	@Transactional
	void updateticket_testcase4() {

		log.info("Start test: updateticket_testcase4");
		
		TicketInfo ticket = ticketService.updateTicket(id4, rejected);
		log.info(ticket.toString());
		
		assertNotNull(ticket, "ticket must be not null");
		assertEquals(id4, ticket.getId(), "ID must be unique.");
		assertEquals(rejected, ticket.getStatus(), "Status must be unique.");
		
		log.info("exec updateticket_testcase4 Service Success!");
	}
	
	@Test
	@Transactional
	void updateticket_testcase5() {

		log.info("Start test: updateticket_testcase5");
		
		TicketInfo ticket = ticketService.updateTicket(id99, pending);
		assertNull(ticket, "ticket must be null");
		
		log.info("exec updateticket_testcase5 Service Success!");
	}
}
