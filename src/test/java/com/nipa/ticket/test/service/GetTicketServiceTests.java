package com.nipa.ticket.test.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import com.nipa.ticket.test.dto.TicketInfo;
import com.nipa.ticket.test.dto.TicketStatus;

@SpringBootTest
public class GetTicketServiceTests {

	private static final Logger log = LoggerFactory.getLogger(AddTicketServiceTests.class);
	
	private static final Integer offset = 0;
	private static final Integer limit = 10;
	private static final String sort = "-name";
	private static final String start = "2021-02-07 13:40:02";
	private static final String end = "2021-02-07 14:59:02";
	
	private static final TicketStatus pending = TicketStatus.pending;
	private static final TicketStatus accepted = TicketStatus.accepted;
	private static final TicketStatus resolved = TicketStatus.resolved;
	private static final TicketStatus rejected = TicketStatus.rejected;
	List<TicketStatus> status = null;

	@Autowired
	@Qualifier("ticket/TicketServiceImpl")
	private ITicketService ticketService;
	
	@BeforeEach
	void setUp() throws Exception {
		MDC.put("suite", "GetTicketServiceTests");
		
		status = new ArrayList<TicketStatus>();
		status.add(accepted);
		status.add(rejected);
	}

	@AfterEach
	void tearDown() throws Exception {
	}
	
	@Test
	@Transactional
	void getticket_testcase1() {

		log.info("Start test: getticket_testcase1");
		
		List<TicketInfo> ticket = ticketService.getTicket(null, null, null, null, null, null);
		
		assertNotNull(ticket, "ticket must be not null");
		ticket.forEach(t -> {
			log.info(t.toString());
			assertNotNull(t.getId(), "ID must be not null");
			assertNotNull(t.getName(), "Name must be not null");
			assertNotNull(t.getStatus(), "Status must be not null");
			assertNotNull(t.getCreateDate(), "Create date must be not null");
			assertNotNull(t.getUpdateDate(), "Update date must be not null");
		});
		
		log.info("exec getticket_testcase1 Service Success!");
	}
	
	@Test
	@Transactional
	void getticket_testcase2() {

		log.info("Start test: getticket_testcase2");
		
		List<TicketInfo> ticket = ticketService.getTicket(status, null, null, null, null, null);
		
		assertNotNull(ticket, "ticket must be not null");
		
		Boolean check = false;
		for(TicketInfo t : ticket) {
			log.info(t.toString());
			assertNotNull(t.getId(), "ID must be not null");
			assertNotNull(t.getName(), "Name must be not null");
			assertNotNull(t.getStatus(), "Status must be not null");
			assertNotNull(t.getCreateDate(), "Create date must be not null");
			assertNotNull(t.getUpdateDate(), "Update date must be not null");

			if(status.contains(t.getStatus())) {
				check = true;
			}
			assertTrue(check, "Status must be unique.");
		}
		
		log.info("exec getticket_testcase2 Service Success!");
	}
}
