package com.nipa.ticket.test.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import com.nipa.ticket.test.dto.TicketBody;
import com.nipa.ticket.test.dto.TicketInfo;
import com.nipa.ticket.test.dto.TicketStatus;

@SpringBootTest
public class AddTicketServiceTests {

	private static final Logger log = LoggerFactory.getLogger(AddTicketServiceTests.class);
	
	TicketBody body = null;
	private static final String name = "test012";
	private static final String detail = "For test.";
	private static final String contact = "08x-xxx-xxxx";
	private static final TicketStatus pending = TicketStatus.pending;

	@Autowired
	@Qualifier("ticket/TicketServiceImpl")
	private ITicketService ticketService;
	
	@BeforeEach
	void setUp() throws Exception {
		
		body = new TicketBody();
		body.setName(name);
		body.setDetail(detail);
		body.setContact(contact);
	}

	@AfterEach
	void tearDown() throws Exception {
	}
	
	@Test
	@Transactional
	void addticket_testcase1() {

		log.info("Start test: addticket_testcase1");
		
		TicketInfo ticket = ticketService.addTicket(body);
		
		assertNotNull(ticket, "ticket must be not null");
		log.info(ticket.toString());
		
		assertEquals(name, ticket.getName(), "Name must be unique.");
		assertEquals(detail, ticket.getDetail(), "Detail must be unique.");
		assertEquals(contact, ticket.getContact(), "Contact must be unique.");
		assertEquals(pending, ticket.getStatus(), "Status must be unique.");
		
		log.info("exec addticket_testcase1 Service Success!");
	}
	
}
